package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.Jc
import org.docx4j.wml.JcEnumeration
import org.docx4j.wml.P
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.WordConstants
import org.somda.reqif.ieee.rendering.createPPr
import org.somda.reqif.ieee.rendering.createParagraph
import org.somda.reqif.ieee.rendering.hasAnotherContentElement
import org.somda.reqif.ieee.rendering.plugin.ElementPlugin
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.w3c.dom.Node

// currently unused as it must not generate the same behavior as a div

class Span(
    private val document: WordprocessingMLPackage,
    private val bookmarks: Map<String, Bookmark>
) : ElementPlugin {
    override fun handledQName() = XhtmlConstants.ELEM_SPAN

    override fun handle(pluginContext: PluginContext): ContentAccessor {
        var cAccess = pluginContext.contentAccessor
        for (i in 0 until pluginContext.source.childNodes.length) {
            val currentNode = pluginContext.source.childNodes.item(i)
            when (currentNode.nodeType) {
                Node.ELEMENT_NODE -> cAccess =
                    pluginContext.runRenderer(
                        pluginContext.reqIfNode,
                        currentNode,
                        pluginContext.parentStyle,
                        pluginContext.defaultPStyle
                    ).renderTo(cAccess)
                Node.CDATA_SECTION_NODE, Node.TEXT_NODE -> {
                    var text = currentNode.textContent
                    if (i == 0) {
                        text = text.trimStart()
                    }
                    if (i == pluginContext.source.childNodes.length - 1) {
                        text = text.trimEnd()
                    }
                    ElementPlugin.appendPlainRun(
                        document.mainDocumentPart, bookmarks, text,
                        pluginContext.copy(contentAccessor = cAccess)
                    )
                }
            }
        }

        return cAccess
    }
}