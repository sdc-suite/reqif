package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.jaxb.Context
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.somda.reqif.ieee.hook.Bookmark
import org.somda.reqif.ieee.rendering.Ids
import org.somda.reqif.ieee.rendering.WordConstants
import org.somda.reqif.ieee.rendering.createPPr
import org.somda.reqif.ieee.rendering.plugin.PluginContext
import org.somda.reqif.ieee.spectype.obj.AnnexType
import org.somda.reqif.ieee.spectype.obj.Heading
import org.somda.reqif.tree.ReqIfNode


abstract class HeadingX(
    document: WordprocessingMLPackage,
    bookmarks: Map<String, Bookmark>
) : Div(document, bookmarks) {
    abstract fun depth(): Int

    override fun applyStyle(pluginContext: PluginContext) {
        val p = pluginContext.contentAccessor as P

        val styleTemplate = when (isAnnex(pluginContext.reqIfNode)) {
            true -> WordConstants.STYLE_TEMPLATE_ANNEX_HEADING_LEVEL
            false -> WordConstants.STYLE_TEMPLATE_HEADING_LEVEL
        }

        val offsetDepth = findOffsetDepth(pluginContext.reqIfNode)

        p.pPr = createPPr(styleTemplate.format(offsetDepth + depth() + 1))
    }

    override fun handle(pluginContext: PluginContext): ContentAccessor {

        val hXId = pluginContext.source.attributes.getNamedItem("id")?.textContent?.trim() ?: ""

        if (hXId.isNotEmpty()) {
            println("Process headline element with id: '$hXId'")
            val id = Ids.nextAsBigInt()
            val factory = Context.getWmlObjectFactory()
            // Add bookmark start
            val bm = factory.createCTBookmark()
            bm.id = id
            bm.name = Heading.bookmarkFor(hXId).name
            val bmStart = factory.createBodyBookmarkStart(bm)
            pluginContext.contentAccessor.content.add(bmStart)
            val result = super.handle(pluginContext)
            val mr = factory.createCTMarkupRange()
            mr.id = id
            val bmEnd = factory.createBodyBookmarkEnd(mr)
            pluginContext.contentAccessor.content.add(bmEnd)
            // Add bookmark end
            return result
        }

        return super.handle(pluginContext)
    }

    private fun isAnnex(reqIfNode: ReqIfNode): Boolean =
        when (reqIfNode.parent) {
            null -> reqIfNode.theObject is AnnexType
            else -> reqIfNode.theObject is AnnexType || isAnnex(reqIfNode.parent)
        }

    // could be more sophisticated if headings in VoidTypes or OverviewTypes are accepted
    private fun findOffsetDepth(reqIfNode: ReqIfNode): Int = reqIfNode.parent?.theObject?.depth ?: 0
}