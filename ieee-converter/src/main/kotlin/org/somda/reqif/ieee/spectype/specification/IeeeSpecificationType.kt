package org.somda.reqif.ieee.spectype.specification

import org.somda.reqif.ieee.datatype.*

data class IeeeSpecificationType(
    val description: IeeePlainString = "IEEE Standard",
    val designation: IeeePlainString = "<designation>",
    val draftNumber: IeeeDraftNumber = 1,
    val trialUse: IeeeTrialUse = false,
    val gdeRecPracStd: IeeeStandardType = IeeeStandardType.STANDARD,
    val parTitle: IeeePlainString = "<Complete Title Matching PAR>",
    val commiteeName: IeeePlainString = "<Commitee Name>",
    val societyName: IeeePlainString = "<Society Name>",
    val dateApproved: IeeeDate = "",
    val draftMonth:IeeeMonth = IeeeMonth.JANUARY,
    val draftYear: IeeeYear = 2020,
    val datePublished: IeeeDate = "",
    val pdfIsbn: IeeeIsbn = "978-0-XXXX-XXXX-X",
    val stdXxxxxx: IeeePlainString = "STDXXXXX",
    val printIsbn: IeeeIsbn = "978-0-XXXX-XXXX-X",
    val stdpdXxxxxx: IeeePlainString = "STDPDXXXXX",
    val workingGroupName: IeeePlainString = "Working Group Name"
)