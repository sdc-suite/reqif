package org.somda.reqif.ieee.pocspec

import org.somda.reqif.ieee.spectype.obj.NoteType
import org.somda.reqif.ieee.spectype.obj.RequirementType
import org.somda.reqif.tree.ReqIfNode
import org.somda.reqif.tree.ReqIfTree
import org.w3c.dom.Node
import java.io.File
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

class PocSpecXmlExport(
    private val reqIfFile: File,
    private val outputDirectory: File?,
    private val overwriteExistingFile: Boolean,
    private val reqIfTree: ReqIfTree
) {
    private val docBuilder = DocumentBuilderFactory.newInstance().also { it.isNamespaceAware = true }
    private val document = docBuilder.newDocumentBuilder().newDocument()

    companion object {
        private const val ELEM_REQUIREMENTS = "Requirements"
        private const val ELEM_REQUIREMENT = "Requirement"
        private const val ELEM_NOTE = "Note"
        private const val ATTR_ID = "id"
    }

    fun writeFile() {
        val pocSpecFilename = reqIfFile.nameWithoutExtension + ".xml"
        val outputFile = File(outputDirectory ?: reqIfFile.parentFile, pocSpecFilename)

        if (outputFile.exists() && !overwriteExistingFile) {
            System.err.println("Output file $outputFile exists already. Use -f option to force overwrite.")
            return
        }

        val reqIfRequirements = (reqIfTree.groups[RequirementType::class] ?: emptyList())

        val xmlRoot = document.createElement(ELEM_REQUIREMENTS)
        document.appendChild(xmlRoot)

        reqIfRequirements.forEach { appendRequirements(it, xmlRoot) }

        val factory = TransformerFactory.newInstance();
        val transformer = factory.newTransformer()
        val domSource = DOMSource(document)
        val streamResult = StreamResult(outputFile)
        transformer.transform(domSource, streamResult)
    }

    private fun appendRequirements(reqIfNode: ReqIfNode, xmlRoot: Node) {
        if (reqIfNode.theObject is RequirementType) {
            val reqElement = document.createElement(ELEM_REQUIREMENT)
            reqElement.setAttribute(ATTR_ID, reqIfNode.theObject.value.tag())
            val xhtmlContent = document.importNode(reqIfNode.theObject.value.text, true)
            reqElement.appendChild(xhtmlContent)
            xmlRoot.appendChild(reqElement)
            reqIfNode.children.forEach { appendNotes(it, reqElement) }
            return
        }

        reqIfNode.children.forEach { appendRequirements(it, xmlRoot) }
    }

    private fun appendNotes(reqIfNode: ReqIfNode, reqNode: Node) {
        if (reqIfNode.theObject is NoteType) {
            val noteElement = document.createElement(ELEM_NOTE)
            val xhtmlContent = document.importNode(reqIfNode.theObject.value.text, true)
            noteElement.appendChild(xhtmlContent)
            reqNode.appendChild(noteElement)
        }
    }
}