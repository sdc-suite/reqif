package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor
import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.WordSimpleStringRenderer
import org.somda.reqif.ieee.spectype.obj.KeywordType
import org.somda.reqif.tree.ReqIfNode

class KeywordHook(private val reqIfNodes: Collection<ReqIfNode>) :
    BookmarkHook {
    override fun label() = ObjectTypeName.KEYWORD.reqifName

    override fun callback(bookmark: CTBookmark) {
        if (reqIfNodes.isEmpty()) {
            System.err.println("No keywords found")
            return
        }

        WordSimpleStringRenderer(reqIfNodes.joinToString { (it.theObject as KeywordType).value.value })
            .renderTo(bookmark.parent as ContentAccessor)
    }
}