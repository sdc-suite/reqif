package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import org.somda.reqif.ieee.datatype.ListType
import org.somda.reqif.ieee.hook.Bookmark


class UnorderedList(
    document: WordprocessingMLPackage,
    bookmarks: Map<String, Bookmark>
) : ListHandler(document, bookmarks, ListType.DASHED) {
    override fun handledQName() = XhtmlConstants.ELEM_UL
}