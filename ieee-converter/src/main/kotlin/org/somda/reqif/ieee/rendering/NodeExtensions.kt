package org.somda.reqif.ieee.rendering

import org.w3c.dom.Node
import javax.xml.namespace.QName

fun Node.isA(qName: QName): Boolean = localName != null && qName == qName()

fun Node.findFirstChild(nodeName: QName): Node? {
    for (i in 0 until childNodes.length) {
        val child = childNodes.item(i)
        if (child.isA(nodeName)) {
            return child
        }
    }
    return null
}

fun Node.qName() = QName(namespaceURI, localName)

fun Node.withoutWhitespaces() = when (textContent.trim().isEmpty()) {
    true -> null
    false -> this
}
