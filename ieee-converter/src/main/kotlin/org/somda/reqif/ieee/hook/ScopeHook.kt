package org.somda.reqif.ieee.hook

import org.docx4j.jaxb.Context
import org.docx4j.wml.CTBookmark
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.P
import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.spectype.obj.ObjectType
import org.somda.reqif.ieee.rendering.WordRenderer
import org.somda.reqif.ieee.rendering.createParagraph
import org.somda.reqif.tree.ReqIfNode

class ScopeHook(
    private val reqIfNodes: Collection<ReqIfNode>,
    private val renderer: (obj: ObjectType) -> WordRenderer
) : BookmarkHook {
    override fun label() = "null" // ObjectTypeName.SCOPE.reqifName

    override fun callback(bookmark: CTBookmark) {
        if (reqIfNodes.isEmpty()) {
            System.err.println("No scope data found")
            return
        }

        val firstParagraph = bookmark.parent as P
        val factory = Context.getWmlObjectFactory()
        val topLevelContentAccessor = firstParagraph.parent as ContentAccessor
        val offset = topLevelContentAccessor.content.indexOf(firstParagraph)

        for ((index, reqIfNode) in reqIfNodes.withIndex()) {
            val nextParagraph = topLevelContentAccessor.createParagraph()
            topLevelContentAccessor.content.add(offset + index + 1, nextParagraph)
            renderer(reqIfNode.theObject).renderTo(nextParagraph!!)
        }
    }
}