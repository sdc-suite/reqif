package org.somda.reqif.ieee.datatype

enum class IeeeSubType(reqifName: String) {
    NONE("None"),
    REVISION("Revision"),
    AMMENDMENT("Ammendment"),
    CORRIGENDUM("Corrigendum")
}