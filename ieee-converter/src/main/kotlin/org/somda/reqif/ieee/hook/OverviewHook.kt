package org.somda.reqif.ieee.hook

import org.docx4j.wml.CTBookmark
import org.docx4j.wml.P
import org.somda.reqif.ieee.datatype.ObjectTypeName
import org.somda.reqif.ieee.rendering.NestedSectionsRendering
import org.somda.reqif.ieee.rendering.Renderers
import org.somda.reqif.ieee.spectype.obj.ObjectType
import org.somda.reqif.ieee.rendering.WordRenderer
import org.somda.reqif.tree.ReqIfNode

class OverviewHook(
    private val reqIfNodes: Collection<ReqIfNode>,
    private val renderers: Renderers
) : BookmarkHook {
    override fun label() = ObjectTypeName.OVERVIEW.reqifName

    override fun callback(bookmark: CTBookmark) {
        if (reqIfNodes.isEmpty()) {
            System.err.println("No overview found")
            return
        }

        val reqIfNode = reqIfNodes.iterator().next()
        NestedSectionsRendering(
            firstParagraph = bookmark.parent as P,
            reqIfNodes = reqIfNode.children,
            renderers = renderers,
            createPagebreaks = false,
            depthOffset = reqIfNode.theObject.depth
        )
//        val firstParagraph = bookmark.parent as P
//        val factory = Context.getWmlObjectFactory()
//        val topLevelContentAccessor = firstParagraph.parent as ContentAccessor
//        val offset = topLevelContentAccessor.content.indexOf(firstParagraph)
//
//        for ((index, reqIfNode) in reqIfNodes.children.withIndex()) {
//            val nextParagraph = factory.createP()
//            topLevelContentAccessor.content.add(offset + index + 1, nextParagraph)
//            renderer(reqIfNode.theObject).renderTo(nextParagraph!!)
//        }
    }
}