package org.somda.reqif.ieee.datatype

import java.lang.Exception

enum class Normativity(val reqIfName: String) {
    NORMATIVE("Normative"),
    INFORMATIVE("Informative");

    companion object {
        private val map = values().associateBy(Normativity::reqIfName)
        fun fromReqIfName(reqIfName: String) = map[reqIfName]
            ?: throw Exception("Enum $reqIfName could not be mapped to ${Normativity::class.java}")
    }
}