package org.somda.reqif.ieee.rendering.xhtml

import org.docx4j.wml.BooleanDefaultTrue
import org.docx4j.wml.RPr

class Bold : BasicStylePlugin() {
    override fun handledQName() = XhtmlConstants.ELEM_BOLD
    override fun decorate(rPr: RPr) {
        rPr.b = BooleanDefaultTrue()
    }
}