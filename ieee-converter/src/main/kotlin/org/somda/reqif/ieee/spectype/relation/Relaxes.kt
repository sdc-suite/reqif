package org.somda.reqif.ieee.spectype.relation

data class Relaxes(val description: RelationDescription = RelationDescription())