package org.somda.reqif.ieee.rendering

import org.docx4j.wml.JcEnumeration
import org.docx4j.wml.PPr
import org.somda.reqif.ieee.datatype.ListType
import org.somda.reqif.ieee.spectype.obj.*

fun ObjectType.toWordPPr(): PPr = when (this) {
    is ComputerCodeType -> createPPr(WordConstants.STYLE_COMPUTER_CODE, JcEnumeration.LEFT)
    is PictureType -> createPPr(WordConstants.STYLE_PARAGRAPH, JcEnumeration.CENTER)
    is ListItemType -> when (this.value.listType) {
        ListType.BULLETED, ListType.DASHED -> createPPr(WordConstants.STYLE_UNORDERED_LIST)
        ListType.LATIN, ListType.NUMBERED ->
            // No support for nested numbered lists yet
            createPPr(WordConstants.STYLE_TEMPLATE_NUMBERED_LIST_LEVEL.format("1"))
    }
    is ParagraphType -> createPPr(WordConstants.STYLE_PARAGRAPH)
    else -> createPPr(WordConstants.STYLE_PARAGRAPH)
}